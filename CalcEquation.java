package com.j2core.mrybalkin.week02.equationsolving;

/**
 * Created by mrybalkin on 5/20/16.
 *
 * Task: Create simple project, only main(), solve equation “ax^2 + bx + c = 0”, check-in, create review
 */
public class CalcEquation {

    public static void main(String[] args) {
        double a = 4.56;
        double b = 7.89;
        double c = 1.23;
        double x1, x2, discriminant;
        double error = 0.00001; //adding to avoid rounding errors in floating-point numbers

        if (a > 0) {
            discriminant = b*b - 4*a*c;

            if (discriminant > error) {
                x1 = (-b + Math.sqrt(discriminant)) / 2 * a;
                x2 = (-b - Math.sqrt(discriminant)) / 2 * a;

                System.out.println("Result is:\n x1 = " + x1 + "\n x2 = " + x2);
            } else if (Math.abs(discriminant) <= error){
                x1 = -b / 2 * a;

                System.out.println("Equation has two similar roots x1, x2 = " + x1);
            }
            else {
                System.out.println("Equation doesn't have roots because discriminant < 0");
            }
        }
    }
}
