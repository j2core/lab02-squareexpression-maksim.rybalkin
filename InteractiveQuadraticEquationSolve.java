package com.j2core.mrybalkin.week02.interactiveequationsolving;

import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by mrybalkin on 5/24/16.
 *
 * 1. Create simple project, only main(), solve equation “ax^2 + bx + c = 0”
 * 2. Add input from terminal, check-in, create review
 */
public class InteractiveQuadraticEquationSolve {
    private static final double ERROR = 0.00001; //adding to avoid rounding errors in floating-point numbers

    public static void main(String[] args) {
        double x1, x2;
        double a = interactiveConsoleInput("a");

        if (a != 0) {
            double b = interactiveConsoleInput("b");
            double c = interactiveConsoleInput("c");

            if (b == 0 && c ==0 ) {
                System.out.println("Variable a and b = 0. So result is x = 0.");
            }else {
                double discriminant = b * b - 4 * a * c;

                if (discriminant >= ERROR) {
                    x1 = (-b - Math.sqrt(discriminant)) / (2 * a);
                    x2 = (-b + Math.sqrt(discriminant)) / (2 * a);

                    System.out.println("Result is:\n x1 = " + x1 + "\n x2 = " + x2);
                } else if (Math.abs(discriminant) < ERROR) {
                    x1 = -b / 2 * a;

                    System.out.println("Equation has two similar roots x1, x2 = " + x1);
                } else {
                    System.out.println("Equation doesn't have roots because discriminant < 0");
                }
            }
        } else {
            System.out.println("Variable a is = 0. It is not quadratic equation");
        }
    }

    /**
     * Ask user to enter a number and parse it to double.
     * If user enters not a number ask user enter again.
     * Catch InputMismatchException.
     *
     * @return double entered in console
     */
    private static double interactiveConsoleInput(String var){

        while(true){
            try {
                System.out.println("Please enter a number for " + var + ":");

                return new Scanner(System.in).nextDouble();
            } catch (InputMismatchException ie){
                System.out.println("You entered not a number, please enter again:");
            }
        }
    }
}
